package su.common.logging.config;

public class CommonConfig implements Config {

    private String rootPath;
    private String logPath;
    private String logFile;
    private long dataSizeLimit;
    private long logSizeLimit;
    private long scanPause;

    public String getRootPath() {
        return this.rootPath;
    }

    public void setRootPath(String rootPath) {
        this.rootPath = rootPath;
    }

    public String getLogPath() {
        return this.logPath;
    }

    public void setLogPath(String logPath) {
        this.logPath = logPath;
    }

    public String getLogFile() {
        return this.logFile;
    }

    public void setLogFile(String logFile) {
        this.logFile = logFile;
    }

    public long getDataSizeLimit() {
        return this.dataSizeLimit;
    }

    public void setDataSizeLimit(long dataSizeLimit) {
        this.dataSizeLimit = dataSizeLimit;
    }

    public long getLogSizeLimit() {
        return this.logSizeLimit;
    }

    public void setLogSizeLimit(long logSizeLimit) {
        this.logSizeLimit = logSizeLimit;
    }

    public long getScanPause() {
        return scanPause;
    }

    public void setScanPause(long scanPause) {
        this.scanPause = scanPause;
    }
}
