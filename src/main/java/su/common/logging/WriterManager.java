package su.common.logging;

import org.apache.commons.io.FileSystemUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import su.common.logging.config.CommonConfig;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

/**
 * Data & logs writing with disk free space control
 */

public class WriterManager {
    private final CommonConfig commonConfig;
    private ScannerManager scannerManager = null;
    private static final Logger log = Logger.getLogger(WriterManager.class);

    public WriterManager(CommonConfig config) {
        this.commonConfig = config;
        initLogProps(config);
        try {
            this.scannerManager = new ScannerManager(config);
        } catch (IOException e) {
            try {
                log(e);
                return;
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }

    /**
     * @param fileName      file name (NOT absolute path)
     * @param input         InputStream instance
     * @return              bytes amount (if operation is completed successfully)
     *                      -1 otherwise
     */
    public long writeToDisk(String fileName, InputStream input) throws IOException{
        return writeToDisk(fileName, getInputBytes(input));
    }

    /**
     * Writing byte array to disk
     *
     * @param fileName      file name (NOT absolute path)
     * @param inputBytes    <code>byte[]</code> array
     * @return              bytes amount (if operation is completed successfully)
     *                      -1 otherwise
     */
    public long writeToDisk(String fileName, byte[] inputBytes) throws IOException{
        if (!scannerManager.existFreeSpaceForData()) {
            logError("Disk free space less than dataSizeLimit value!" +
                    "Stop writing data..");
            return -1;
        }
        String outputFilePath = commonConfig.getRootPath() + fileName;
        File outputFile = new File(outputFilePath);
        try {
            createDirectoriesInPath(outputFilePath);
            outputFile.createNewFile();
        } catch (IOException e) {
            log(e);
            return -1;
        }
        long freeSpace = 0;
        try {
            freeSpace = FileSystemUtils.freeSpaceKb(outputFilePath) * 1024;
        } catch (IOException e) {
            log(e);
            return -1;
        }
        if(inputBytes.length > freeSpace - commonConfig.getDataSizeLimit()) {
            logError("input stream size more than dataSizeLimit! value" +
                    " Stop writing data..");
            return -1;
        }
        if(outputFile.isDirectory()) {
            return inputBytes.length;
        }
        
        FileOutputStream outputStream = null;
        try {
            outputStream = new FileOutputStream(outputFile);
            outputStream.write(inputBytes);
            outputStream.flush();
            outputStream.close();
        } catch (IOException ex) {
            log(ex);
            return -1;
        } finally {
            try {
                if (outputStream != null) {
                    outputStream.close();
                }
            } catch (IOException ex) {
                throw ex;
            }
        }
        return inputBytes.length;
    }

    /**
     * Logging with INFO level
     *
     * @param str   message to logging
     * @throws      IOException
     */
    public void log(String str) throws IOException {
        log(new ByteArrayInputStream(str.getBytes("UTF-8")));
    }

    /**
     * Logging with INFO level
     *
     * @param input     InputStream instance
     * @throws          IOException
     */
    public void log(InputStream input) throws IOException {
        if(!scannerManager.existFreeSpaceForLog()) {
            throw new IOException("Disk free space size less than logSizeLimit value! " +
                    "Stop writing data..");
        }
        byte[] inputBytes = getInputBytes(input);
        if (inputBytes.length > commonConfig.getLogSizeLimit()) {
            throw new IOException("InputStream size more than logSizeLimit value! " +
                    "Stop writing data..");
        }
        log.info(new String(inputBytes));
    }

    /**
     * Logging with ERROR level
     *
     * @param message   message to logging
     * @throws IOException
     */
    public void logError(String message) throws IOException {
        if(!scannerManager.existFreeSpaceForLog()) {
             throw new IOException("Disk free space size less than logSizeLimit value! " +
                     "Stop writing data..");
        }
        byte[] inputBytes = new byte[0];
        try {
            inputBytes = getInputBytes(new ByteArrayInputStream(message.getBytes("UTF-8")));
        } catch (UnsupportedEncodingException e) {
            log(e);
        }
        if (inputBytes.length > commonConfig.getLogSizeLimit()) {
            throw new IOException("Message size more than logSizeLimit value!" +
                    "Stop writing data..");
        }
        log.error(new String(inputBytes));
    }

    /**
     * Logging exceptions
     *
     * @param ex    exception to logging
     *              -1 otherwise
     */
    public void log(Exception ex) throws IOException{
        StringWriter sw = new StringWriter();
        ex.printStackTrace(new PrintWriter(sw));
        logError(sw.toString());
    }

    /**
     * Init log4j-properties on the base of values from .json config file
     *
     * @param config
     */
    private void initLogProps(CommonConfig config) {
        Properties props = new Properties();
        try {
            InputStream inStream = this.getClass().getResourceAsStream("/log4j.properties");
            props.load(inStream);
            inStream.close();
        } catch (Exception e){
            try {
                log(e);
            } catch (IOException e1) {
                return;
            }
        }
        props.setProperty("log", config.getLogPath());
        props.setProperty("file.name", config.getLogFile());
        PropertyConfigurator.configure(props);
    }

    /**
     * Converting InputStream -> byte[]
     *
     * @param input     InputStream instance
     * @return          <code>byte[]</code>
     * throws IOException
     */
    private byte[] getInputBytes(InputStream input) throws IOException{
        byte[] inputBytes = null;
        try {
            inputBytes = IOUtils.toByteArray(input);
        } catch(IOException ex) {
            log(ex);
        }
        return inputBytes;
    }

    /**
    * Creates necessary catalogs for writing
    *
    * @param path   absolute path to file
    * @throws IOException
    */
    private void createDirectoriesInPath(String path) throws IOException {
        String directories = FilenameUtils.getFullPath(path);
        Path dirs = Paths.get(directories);
        Files.createDirectories(dirs);
    }
}
