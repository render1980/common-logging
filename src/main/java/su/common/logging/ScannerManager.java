package su.common.logging;

import org.apache.commons.io.FileSystemUtils;
import su.common.logging.config.CommonConfig;
import java.io.File;
import java.io.IOException;

/**
 * Provide methods for disk free space control
 */
public class ScannerManager {
    private final CommonConfig config;

    public ScannerManager(CommonConfig config) throws IOException {
        this.config = config;
        checkDataPaths();
        checkLogPaths();
    }

    public CommonConfig getConfig() {
        return this.config;
    }

    /**
     * Check disk free space for data writing
     *
     * @return  <code>true</code> if free space is enough
     */
    public boolean existFreeSpaceForData() throws IOException {
        long freeSpace = FileSystemUtils.freeSpaceKb(config.getRootPath()) * 1024;
        return freeSpace > config.getDataSizeLimit();
    }

    /**
     * Check disk free space for logs writing
     *
     * @return  <code>true</code> if free space is enough
     */
    public boolean existFreeSpaceForLog() throws IOException {
        long freeSpace = FileSystemUtils.freeSpaceKb(config.getLogPath()) * 1024;
        return freeSpace > config.getLogSizeLimit();
    }

    /**
     * Create catalogs and file for logging
     */
    private void checkLogPaths() throws IOException {
        String logPath = config.getLogPath();
        File logDir = new File(logPath);
        File logFile = new File(logPath + config.getLogFile());
        if (!logDir.exists())
            logDir.mkdirs();
        if (!logFile.exists())
            logFile.createNewFile();
    }

    /**
     * Create catalogs and file for data writing
     */
    private void checkDataPaths() {
        String path = config.getRootPath();
        File dir = new File(path);
        if (!dir.exists())
            dir.mkdirs();
    }
}
