package su.common.logging;

import com.fasterxml.jackson.databind.ObjectMapper;
import su.common.logging.config.Config;
import java.io.File;
import java.io.IOException;

public class ConfigReader {
    /**
     * Read configuration file and initialize object which implements Config interface
     *
     * @param filePath Absolute path to config file
     * @param clazz    Class implements Config interface
     * @return         Instance of class which implements Config interface
     */
    public static <T extends Config> T read(String filePath, Class<T> clazz) throws IOException{
        return new ObjectMapper().readValue(new File(filePath), clazz);
    }
}
