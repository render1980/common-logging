package su.common.logging;

import org.apache.commons.io.IOUtils;
import org.apache.maven.surefire.shade.org.apache.maven.shared.utils.io.FileUtils;
import org.junit.Before;
import org.junit.Test;
import su.common.logging.config.CommonConfig;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * @author Dmitry Boltnev
 * Date: 12/12/13
 * Time: 8:09 PM
 */
public class ScannerManagerTest {
    private ScannerManager sm;
    private static final String PREFIX = "." + IOUtils.DIR_SEPARATOR;
    private static final String ROOT_PATH = PREFIX + "target" + IOUtils.DIR_SEPARATOR + "test-out" + IOUtils.DIR_SEPARATOR;
    private static final String CONFIG_NAME = "common-config-test.conf";
    private static final String CONFIG_TEST_PATH = ROOT_PATH + CONFIG_NAME;

    @Before
    public void setUp() {
        File testPath = new File(ROOT_PATH);
        File testConfig = new File(CONFIG_TEST_PATH);
        if (!testPath.exists()) {
            testPath.mkdirs();
        }
        if (!testConfig.exists()) {
            try {
                Files.copy(Paths.get(PREFIX + CONFIG_NAME), testConfig.toPath());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        CommonConfig config = null;
        try {
            config = ConfigReader.read(CONFIG_TEST_PATH, CommonConfig.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            sm = new ScannerManager(config);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void shouldExistsPathsAfterScannerManagerInitialized() {
        CommonConfig commonConfig = sm.getConfig();
        String rootPath = commonConfig.getRootPath();
        String logPath = commonConfig.getLogPath();
        String logFile = commonConfig.getLogFile();
        assert(FileUtils.fileExists(rootPath) && FileUtils.fileExists(logPath + logFile));
    }

    @Test
    public void testExistFreeSpaceForData() throws Exception {
        assert(sm.existFreeSpaceForData());
    }

    @Test
    public void testExistFreeSpaceForLog() throws Exception {
        assert(sm.existFreeSpaceForLog());
    }
}
