package su.common.logging;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;
import su.common.logging.config.CommonConfig;
import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * @author Dmitry Boltnev
 * Date: 12/10/13
 * Time: 5:45 PM
 */
public class WriterManagerTest{
    private WriterManager wm;
    private static final String PREFIX = "." + IOUtils.DIR_SEPARATOR;
    private static final String ROOT_PATH = PREFIX + "target" + IOUtils.DIR_SEPARATOR + "test-out" + IOUtils.DIR_SEPARATOR;
    private static final String CONFIG_NAME = "common-config-test.conf";
    private static final String CONFIG_TEST_PATH = ROOT_PATH + CONFIG_NAME;
    private CommonConfig config = null;

    @Before
    public void setUp() throws Exception {
        File testPath = new File(ROOT_PATH);
        File testConfig = new File(CONFIG_TEST_PATH);
        if (!testPath.exists()) {
            testPath.mkdirs();
        }
        if (!testConfig.exists()) {
            try {
                Files.copy(Paths.get(PREFIX + CONFIG_NAME), testConfig.toPath());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        config = ConfigReader.read(CONFIG_TEST_PATH, CommonConfig.class);
        // clean paths to log && data
        String rootPath = config.getRootPath();
        String logPath = config.getLogPath();
        File root = new File(rootPath);
        File log = new File(logPath);
        if (root.exists())
            FileUtils.cleanDirectory(root);
        if (log.exists())
            FileUtils.cleanDirectory(log);
        wm = new WriterManager(config);
    }

    /**
     * for writeToDisk method
     */
    @Test
    public void shouldExistFileAfterWriteToDisk() throws Exception {
        String message = "Test of method [writeToDisk] successful";
        InputStream input = new ByteArrayInputStream(message.getBytes("UTF-8"));
        String fileName = "test";
        String rootPath = config.getRootPath();
        Path outputFilePath = Paths.get(rootPath + fileName);
        wm.writeToDisk(fileName, input);

        assert(FileUtils.directoryContains(new File(rootPath),
                new File(outputFilePath.toString())));
        assert(!FileUtils.directoryContains(new File(rootPath),
                new File(outputFilePath.toString() + "wtf")));
    }

    @Test
    public void shouldEqualMessageAfterWriteToDisk() throws Exception{
        String message = "Test of method [writeToDisk] successful";
        InputStream input = new ByteArrayInputStream(message.getBytes("UTF-8"));
        String fileName = "test";
        String rootPath = config.getRootPath();
        Path outputFilePath = Paths.get(rootPath + fileName);
        wm.writeToDisk(fileName, input);

        InputStream inputStream = new FileInputStream(outputFilePath.toString());
        List<String> resLines = IOUtils.readLines(inputStream, Charset.forName("UTF-8"));
        assert(resLines.get(0).compareTo(message) == 0);
    }

    /**
     * for log
     * @throws Exception
     */
    @Test
    public void shouldExistFileAfterLog() throws Exception {
        String message = "Test of method [log] successful";
        InputStream input = new ByteArrayInputStream(message
                .getBytes("UTF-8"));
        String fileName = "/common.log";
        String logPath = config.getLogPath();
        Path outputFilePath = Paths.get(logPath + fileName);
        wm.log(input);

        assert(FileUtils.directoryContains(new File(logPath),
                new File(outputFilePath.toString())));
        assert(!FileUtils.directoryContains(new File(logPath),
                new File(outputFilePath.toString() + "wtf")));

    }

    @Test
    public void shouldEqualMessageAfterLog() throws Exception{
        String message = "Test of method [log] successful";
        InputStream input = new ByteArrayInputStream(message
                .getBytes("UTF-8"));
        String fileName = "/common.log";
        String logPath = config.getLogPath();
        Path outputFilePath = Paths.get(logPath + fileName);
        wm.log(input);

        InputStream inputStream = new FileInputStream(outputFilePath.toString());
        List<String> resLines = IOUtils.readLines(inputStream, Charset.forName("UTF-8"));
        assert(resLines.get(resLines.size() - 1).contains(message));
    }

    @Test
    public void shouldExistFileAfterLogStr() throws Exception {
        String message = "Test of method [log] successful";
        String fileName = "/common.log";
        String logPath = config.getLogPath();
        Path outputFilePath = Paths.get(logPath + fileName);
        wm.log(message);

        assert(FileUtils.directoryContains(new File(logPath),
                new File(outputFilePath.toString())));
        assert(!FileUtils.directoryContains(new File(logPath),
                new File(outputFilePath.toString() + "wtf")));

    }

    @Test
    public void shouldEqualMessageAfterLogStr() throws Exception{
        String message = "Test of method [log] successful";
        String fileName = "/common.log";
        String logPath = config.getLogPath();
        Path outputFilePath = Paths.get(logPath + fileName);
        wm.log(message);

        InputStream inputStream = new FileInputStream(outputFilePath.toString());
        List<String> resLines = IOUtils.readLines(inputStream, Charset.forName("UTF-8"));
        assert(resLines.get(resLines.size() - 1).contains(message));
    }
}