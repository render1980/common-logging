package su.common.logging;

import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;
import su.common.logging.config.CommonConfig;
import su.common.logging.config.Config;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

/**
 * @author Dmitry Boltnev
 * Date: 15.12.13
 * Time: 17:02
 */
public class ConfigReaderTest {
    private static ConfigReader configReader;
    private static final String PREFIX = "." + IOUtils.DIR_SEPARATOR;
    private static final String ROOT_PATH = PREFIX + "target" + IOUtils.DIR_SEPARATOR + "test-out" + IOUtils.DIR_SEPARATOR;
    private static final String CONFIG_NAME = "common-config-test.conf";
    private static final String CONFIG_TEST_PATH = ROOT_PATH + CONFIG_NAME;

    @Before
    public void setUp() {
        configReader = new ConfigReader();
        File testPath = new File(ROOT_PATH);
        File testConfig = new File(CONFIG_TEST_PATH);
        if (!testPath.exists()) {
            testPath.mkdirs();
        }
        if (!testConfig.exists()) {
            try {
                Files.copy(Paths.get(PREFIX + CONFIG_NAME), testConfig.toPath());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * tests with WriterManagerConfig implementation
     */
    @Test
    public void shouldBeCorrectParamRootPathAfterConfigRead() throws Exception {
        String jsonFilePath = CONFIG_TEST_PATH;
        List<String> resLines = IOUtils.readLines(new FileInputStream(jsonFilePath),
                Charset.forName("UTF-8"));

        String line = "";
        for (String str : resLines) {
            if (str.contains("rootPath")) {
                line = str;
                break;
            }
        }
        String val = line.split(":")[1].replace(" ", "").replace("\"", "").replace(",", "");
        Config config = configReader.read(jsonFilePath, CommonConfig.class);
        assert(((CommonConfig)config).getRootPath().compareTo(val) == 0);
    }

    @Test
    public void shouldBeCorrectParamLogPathAfterConfigRead() throws Exception {
        String jsonFilePath = CONFIG_TEST_PATH;
        List<String> resLines = IOUtils.readLines(new FileInputStream(jsonFilePath),
                Charset.forName("UTF-8"));
        String line = "";
        for (String str : resLines) {
            if (str.contains("logPath")) {
                line = str;
                break;
            }
        }
        String val = line.split(":")[1].replace(" ", "").replace("\"", "").replace(",", "");
        Config config = configReader.read(jsonFilePath, CommonConfig.class);
        assert(((CommonConfig)config).getLogPath().compareTo(val) == 0);
    }

    @Test
    public void shouldBeCorrectParamLogFileAfterConfigRead() throws Exception {
        String jsonFilePath = CONFIG_TEST_PATH;
        List<String> resLines = IOUtils.readLines(
                new FileInputStream(jsonFilePath),
                Charset.forName("UTF-8"));
        String line = "";
        for (String str : resLines) {
            if (str.contains("logFile")) {
                line = str;
                break;
            }
        }
        String val = line.split(":")[1].replace(" ", "").replace("\"", "").replace(",", "");
        Config config = configReader.read(jsonFilePath, CommonConfig.class);
        assert(((CommonConfig)config).getLogFile().compareTo(val) == 0);
    }

    @Test
    public void shouldBeCorrectDataSizeAndLogSizeLimitsAfterConfigRead() throws Exception {
        String jsonFilePath = CONFIG_TEST_PATH;
        List<String> resLines = IOUtils.readLines(
                new FileInputStream(jsonFilePath),
                Charset.forName("UTF-8"));
        String dataSizeLimitStr = "";
        String logSizeLimitStr = "";
        for (String str : resLines) {
            if (str.contains("dataSizeLimit"))
                dataSizeLimitStr = str;
            if (str.contains("logSizeLimit"))
                logSizeLimitStr = str;
        }
        String dataSizeLimitVal = dataSizeLimitStr.split(":")[1].replace(" ", "").replace("\"", "")
                .replace(",", "");
        String logSizeLimitVal = logSizeLimitStr.split(":")[1].replace(" ", "").replace("\"", "")
                .replace(",", "");
        Config config = configReader.read(jsonFilePath, CommonConfig.class);

        CommonConfig commonConfig = (CommonConfig) config;
        assert(commonConfig.getDataSizeLimit() == Long.parseLong(dataSizeLimitVal)
                && commonConfig.getLogSizeLimit() == Long.parseLong(logSizeLimitVal));
    }
}
